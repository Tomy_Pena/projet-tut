#ifndef STRUCT_AUTOMATA_H
#define STRUCT_AUTOMATA_H

#include "linked_list.h"
#include "random_a_b.h"
#include <time.h>


typedef struct Automata Automata;

typedef struct State State;

typedef struct Transi Transi;

typedef struct Alphabet	Alphabet;


Alphabet* create_empty_alphabet();

List_t* get_listnb (State* s);

void add_letter (Alphabet* alpha, char* label);

Automata* create_empty_auto ();

State* get_state_by_nb( int n , Automata* a );

void print_state_number (State* s);

void add_state (Automata* a, List_t* nb );

void add_final (Automata* a, State* s);

void add_state_by_a_state (Automata* a, State* state_added);

List_t* get_states (Automata* a);

void add_start (Automata* a, State* s);

void add_transi (State* state, List_t* lab, State* next );

void add_random_transi(Automata* a, int n);

void add_transi_good(Automata* automate);
/*
renvoie une liste d'entiers, correspondant aux états atteints depuis st_looked, avec la lettre lett
*/
List_t* delta (State* st_looked, List_t* lett); 


Automata* determinise(Automata* nda);

Automata* create_auto_random (int n, Alphabet* alphab);

void print_composantes_accessibles(Automata* af);

void print_states_and_transis (Automata* af);

bool is_deterministe( Automata* a );






#endif