
#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

#include "random_a_b.h"


/*
int my_rand (void){
	//srand(time(NULL));
	return (rand());
}*/

int my_rand(void){
 	static int init=0;
 	if (init==0){
 	srandom(time(NULL)-getpid());
  init = 1;
	}
 	return random();
}
