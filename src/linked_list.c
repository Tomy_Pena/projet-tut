#include <stdlib.h>
#include <stdio.h>

#include "linked_list.h"

struct List_t {
  List_t* next;
  void* elem;
};

List_t* creer_liste_vide()
{
  List_t* l = (List_t*)malloc(sizeof(List_t));
  if (l == NULL)
    return NULL;

  return l;
}

List_t* creer_liste(void* elem)
{
  List_t* l = malloc(sizeof(List_t));
  if (l == NULL)
    return NULL;

  l->next = NULL;
  l->elem = elem;

  return l;
}

void* get_elem_from_list(List_t* l) {
  if( l->elem == NULL ){
    printf("element null \n");
  }
  return (l->elem);
}

void supprimer_liste(List_t* l)
{
  if (l == NULL)
    return;

  do {
    List_t* lnext = l->next;
    free(l);
    l=lnext;
  } while (l != NULL);
}

void supprimer_element(List_t* l, int n){
  if (l==NULL){
    return;
  }
  List_t* lit= l;
  if (n>0){
    int i = 0;
    while (i < n-1){
      List_t* lnext = lit->next;
      lit=lnext;
       i++;
    }
    List_t* lsupp = lit->next;
    lit->next = (lit->next)->next;
    free(lsupp);
 }
 else {
   List_t* lfirst = l;
   l=l->next;
   free(lfirst);
 }
}

//recuperer_element(int n)


void ajouter_element(List_t* l, void *elem_to_add)
{
  if(l==NULL){
    printf("l est null\n");
    return;
  }
  if (l->elem == NULL){
    l->elem = elem_to_add;
    return;
  }
  List_t* list_it = l;

  while(list_it->next!=NULL){
    List_t* lnext=list_it->next;
    list_it=lnext;
  }
  List_t* new_l = creer_liste_vide();
  list_it->next = new_l;
  new_l->elem = elem_to_add;
  new_l->next = NULL;
 
}

bool check_elem (List_t* l1, List_t* l2){
  if( l1==NULL ){
    printf("adresse premiere liste nulle \n");
    return false ;
  }
  return (l1->elem == l2-> elem);
}

bool find_int_in_list(int k , List_t* l){

  List_t* l_it=l;
  while(l_it != NULL){
    
    if (*(int*)(l_it->elem) == k)
      return true;
    l_it=l_it->next;
  }
  return false;
}


List_t* fusion_list(List_t* l, List_t* list_added){
  List_t* list_it = list_added;

  if( l->elem == NULL ){
    return list_added ;
  }

  List_t* l_to_return = l ;
  while(list_it != NULL){
    int nb = *(int*)(list_it->elem);
    if( !find_int_in_list(nb,l_to_return) ){
      ajouter_element(l_to_return, (list_it->elem) ) ;
    }
    list_it = list_it->next ;
  }
  return l_to_return ;
}

int size_of_list(List_t* l){
  List_t* l_it=l;
  int n = 0;
  while (l_it != NULL){
    l_it=l_it->next;
    n++;
  }
  return n;
}


List_t* get_next_from_list(List_t* l){
  return l->next;
}

bool check_lists_equals (List_t* l1, List_t* l2){
  
  if (size_of_list(l1) != size_of_list(l2))
    return false;

  List_t* l1_it = l1 ;
  while( l1_it != NULL ){

    int nb_of_l1 = *(int*)(l1_it->elem) ;
    
    if(!find_int_in_list(nb_of_l1,l2) ){
      return false ;
    }
    l1_it = l1_it->next ;
  }
  return true ;

}
