#include <stdlib.h>
#include <stdio.h>

#include "struct_automata.h"
#include "linked_list.h"
#include "random_a_b.h"


int main(int argc, char const *argv[]) {
	int nombre_d_etats = 10;


    Alphabet* alphab_test = create_empty_alphabet();
    char* letterA = (char*)malloc(sizeof(char));
    char letter_A = 'a';
    letterA = &letter_A;
    char* letterB = (char*)malloc(sizeof(char));
    char letter_B = 'b';
    letterB = &letter_B;

    add_letter(alphab_test, (char*)letterA);
    add_letter(alphab_test, (char*)letterB);
    

    Automata* af_test = create_auto_random (nombre_d_etats, alphab_test);
    printf("auto créé \n") ;
    if( !is_deterministe(af_test) )
    	printf( "l'auto est pas deterministe!\n" ) ;
    if (is_deterministe(af_test))
    	printf("oui il est deter\n" );
    print_composantes_accessibles(af_test);

    add_transi_good(af_test);

	Automata* afd = determinise(af_test);
	if( !is_deterministe(afd) )
    	printf( "l'auto est pas deterministe!\n" ) ;
    if (is_deterministe(afd))
    	printf("oui il est deter\n" );

    //add_random_transi(af_test, nombre_d_etats);


    
    //print_states_and_transis (af_test);
    printf("fin main \n");
}
