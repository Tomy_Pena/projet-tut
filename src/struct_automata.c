#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include "struct_automata.h"



/*
 Automate : 
 - liste d'états
 - états entrants : liste de pointeurs vers ces états
 - etats sortants : idem
 - alphabet
*/
struct Automata {
  List_t* states;
  List_t* start;
  List_t* ends ;
  Alphabet* alphabet;
};

/*
état : 
	- first_nb : liste d'entiers (pour un automate normal, seul le premier compte, c'est pour le déterminisisé qu'il faut une liste )
	- transis : bah c'est les transitions qui sortent de l'état quoi
*/
struct State {
  List_t* first_nb;  // liste des numeros dans l'état , utile pour le déterminisé
  List_t* transis;
};

/*
transi : 
- un pointeur vers la liste des lettres de l'alphabet
- un pointeur vers l'état d'arrivée
*/
struct Transi {
    List_t* label ;
    State* next ;
};

/*
alphabet : 
une liste de lettres
*/
struct Alphabet {
  List_t* letters;
};

/*
crée un alphabet vide
*/
Alphabet* create_empty_alphabet(){
  Alphabet* alphabet_empty = malloc(sizeof(Alphabet));
  if (alphabet_empty == NULL)
    return NULL;

  alphabet_empty->letters = creer_liste_vide();
  return alphabet_empty;
}

List_t* get_listnb (State* s){
	return s->first_nb;
}

/*
ajoute une lettre label a l'alphabet alpha
*/
void add_letter (Alphabet* alpha, char* label){
  ajouter_element(alpha->letters,label);
}

/*
ajoute l'état s à la liste des départs de a
*/
void add_start (Automata* a, State* s){
	List_t* state_it = a->states;
	bool founded = (get_elem_from_list(state_it) == s);
	while (state_it != NULL && founded == false){
		state_it = get_next_from_list(state_it);
		founded = (get_elem_from_list(state_it) == s);
	}
  ajouter_element(a->start, state_it);
}

void add_final (Automata* a, State* s){
	List_t* state_it = a->states;
	bool founded = (get_elem_from_list(state_it) == s);
	while (state_it != NULL && founded == false){
		state_it = get_next_from_list(state_it);
		founded = (get_elem_from_list(state_it) == s);
	}
  ajouter_element(a->ends, state_it);
}
/*
crée un automate vide
*/
Automata* create_empty_auto (){
  Automata* a = (Automata*)malloc(sizeof(Automata) ) ;
  if (a==NULL){
    return NULL;
  }

  a->states = creer_liste_vide() ;
  a->start = creer_liste_vide();
  a->ends = creer_liste_vide();
  return a;
}

State* get_state_by_nb(int n, Automata* a){
	List_t* state_it = a->states ;
	State* state_looked = (State*)get_elem_from_list(state_it);

	while (state_it != NULL){
		state_looked = (State*)get_elem_from_list(state_it);
		/*if( (int*)get_elem_from_list(state_looked->first_nb) == NULL )
			printf("probleme sur nb \n");*/
		if( *(int*)get_elem_from_list(state_looked->first_nb) == n ){
			//printf("trouvé =) \n") ;
			return state_looked ;
		}

		state_it = get_next_from_list(state_it);
	}
	//printf("on a pas trouvé \n");
	return NULL;
}

/*
ajoute un état à a, numéroté par la liste nb
*/
void add_state(Automata* a, List_t* nb ){
  State* s = (State*)malloc(sizeof(State));
  s->first_nb = nb ;
  s->transis = creer_liste_vide() ;
  ajouter_element(a->states , s ) ;
}

void add_state_by_a_state (Automata* a, State* state_added){
	ajouter_element(a->states, state_added);
}

List_t* get_states (Automata* a){
	return a->states;
}
/*
affiche le numéro de l'état s (le numéro étant le premier/seul numéro de sa liste de nombres)
*/

void print_state_number (State* s){
	
	int* number = (int*)get_elem_from_list(s->first_nb);
	printf("%d \n", *(number) );
}

/*
ajoute la transi sortante de state , étiqutée par la lettre lab (de la liste de lettres de l'alphabet), vers l'état next,
*/
void add_transi( State* state , List_t* lab , State* next ){
  Transi* transi_to_add = (Transi*)malloc(sizeof(Transi)) ;
  transi_to_add->label = lab ;
  transi_to_add->next = next ;
  if( state->transis == NULL ){
  	printf("la liste de transis est nulle\n") ;
  }
  ajouter_element(state->transis , transi_to_add);
}

void add_random_transi(Automata* a, int n){
	
	int n_rand1 = (my_rand()%n)+1;
  int n_rand2 = (my_rand()%n)+1;
  printf("nombre 1 : %d, nombre 2 : %d\n", n_rand1, n_rand2 );
  
  State* departure = get_state_by_nb(n_rand1, a);
  State* arrival = get_state_by_nb(n_rand2, a);
  printf("n1 = %d , n2 = %d, n = %d \n", n_rand1, n_rand2, n);
  if (departure == NULL)
  	printf("erreur adresse depart\n");
  if (arrival == NULL)
  	printf("erreur adresse arrivee\n");
  add_transi(departure, (a->alphabet)->letters, arrival);
}


void add_transi_good(Automata* automate ){
	State* departure = get_state_by_nb(1,automate);
	State* arrival = get_state_by_nb(2,automate);
	add_transi(departure, (automate->alphabet)->letters, arrival );
	printf("transi ajoutée ! \n");
}
/*
returns the list of the numbers of the states (not the amount), reached from the state st_looked, with the letter lett
*/
List_t* delta (State* st_looked, List_t* lett){

  List_t* trans_it = st_looked->transis;
  List_t* numbers = creer_liste_vide();

  while (trans_it != NULL){
    Transi* transi_looked= get_elem_from_list(trans_it);
    if (transi_looked->label == lett)
      ajouter_element(numbers,(transi_looked->next)->first_nb);
    trans_it=get_next_from_list(trans_it);
  }
  return numbers;
}


/*
déterminise nda
PS : je n'ai pas encore fait l'attribution des états finaux
*/


Automata* determinise(Automata* nda){
  // INITIALSATION
  Automata* adet = create_empty_auto();
  List_t* start_it = nda->start;
  List_t* l = creer_liste_vide();

  // création du premier état, regrouppant tous les chiffres des états initiaux de NDA (auto non déterminisé)
  while(start_it != NULL){
    List_t* list_state_looked = ((List_t*)get_elem_from_list(start_it));
    State* state_looked = (State*)get_elem_from_list(list_state_looked);
    ajouter_element(l, (int*)get_elem_from_list(state_looked->first_nb));
    start_it = get_next_from_list(start_it);
  }

  add_state(adet,l);

  // DETERMINISATION
  List_t* states_it = adet->states; // pour chaque état du nouvel auto
  while(states_it != NULL){
    List_t* letters_it = (nda->alphabet)->letters;// pour chaque lettre de l'alphabet
    while(letters_it != NULL){
    	List_t* l_nbs = creer_liste_vide();  //  liste des états de la futur transi
    	List_t* numbers_it = ((State*)get_elem_from_list(states_it))->first_nb; // liste des nb de l'état où on se situe
    	while (numbers_it != NULL){ // on parcours chacun des chiffres
    		State* etat_indexe_par_le_nb = get_state_by_nb(*(int*)get_elem_from_list(numbers_it), nda); // etat de numero numbers_it
    		List_t* successors = delta( etat_indexe_par_le_nb , letters_it ); 
    		l_nbs = fusion_list(l_nbs, successors ); //on ajoute les numeros des etats atteints depuis l'état, sans rajouter un numero deja existant
    		numbers_it = get_next_from_list(numbers_it);
    	}
    	//regarder si l'état/groupe d'etat est déjà présent :
    	List_t* check_existing_state = adet->states; // iterateur pour regarder si il(s) existe(nt)
    	State* the_state = (State*)get_elem_from_list(check_existing_state); // l'état en question
    	List_t* nb_of_checked = (the_state)->first_nb; // sa liste de nombres
    	bool founded = false;
    	while(check_existing_state!= NULL){
    		
    		founded = check_lists_equals(nb_of_checked, l_nbs);
    		
    		if (founded){
    			printf("if founded \n") ;
    			add_transi(get_elem_from_list(states_it), letters_it,the_state);
    			check_existing_state =NULL;
    			printf("fin if founded \n");
    		}
    		if (!founded && check_existing_state== NULL){ // si on ne trouve pas d'état, et qu'on a parcouru toute la liste
    			printf("pas founded \n") ;
    			add_state(adet, l_nbs); // on le rajoute
    		}
    		check_existing_state=get_next_from_list(check_existing_state);
    	}
    	letters_it=get_next_from_list(letters_it);
    }
    states_it=get_next_from_list(states_it);
  }



  return adet;
}









/*
crée un af déterministe aléatoire
il y anecore des bugs

on crée d'abord les états,
puis on attribue a chaque état une transition par lettre de l'alphabet
*/
Automata* create_auto_random (int n, Alphabet* alphab){
	  Automata* new_af = create_empty_auto();


	  // creation des états
	  int list_nb[n] ; 
	  for (int i = 0; i < n; i++){

	  	list_nb[i] = i+1 ;
	    List_t* nb = creer_liste_vide();
	    ajouter_element(nb, list_nb+i);


	    State* new_state = (State*)malloc(sizeof(State));
	    new_state->first_nb = nb ;
	    new_state->transis = creer_liste_vide();
	    add_state_by_a_state(new_af,new_state) ;
	  }

	  //creation transitions
	  List_t* states_it = new_af->states;
	  if (states_it == NULL)
	  	printf("states it est null \n");
	  while(states_it != NULL){
	    List_t* alphab_it = alphab->letters;
	    while (alphab_it != NULL){
	      
				int n_to_go = (my_rand()%n)+1  ;

	      State* departure = (State*)get_elem_from_list(states_it);
	      State* arrival = get_state_by_nb(n_to_go,new_af);
	      add_transi(departure,alphab_it,arrival);
	      
	      alphab_it = get_next_from_list(alphab_it);
	    }

	    // affichage des etats
	    
	    List_t* transis_du_state = ((State*)get_elem_from_list(states_it))->transis ;
	    Transi* transi_a = (Transi*)get_elem_from_list(transis_du_state);
	    Transi* transi_b = (Transi*)get_elem_from_list(get_next_from_list(transis_du_state));
	    State* state_a = transi_a->next; 
	    State* state_b = transi_b->next;
	    int* nb_a = (int*)get_elem_from_list(state_a->first_nb);
	    int* nb_b = (int*)get_elem_from_list(state_b->first_nb);
	    State* state_louquaid = (State*)get_elem_from_list(states_it);
	    printf("etat : %d , a : %d , b : %d \n",*(int*)get_elem_from_list(state_louquaid->first_nb) ,*nb_a , *nb_b);
	    
	    states_it = get_next_from_list(states_it);

	  }

	  //creation etats initiaux et finaux
	  states_it = new_af->states;
	  int n_rand_starter = (my_rand()%n)+1;
	  State* state_departure = get_state_by_nb(n_rand_starter, new_af);
	  add_start(new_af,state_departure);
	  while(states_it!=NULL){
	  	int n_rand = (my_rand ()%n)+1;
	  	if (n_rand>=(2*n)/3)
	  		add_final(new_af, get_elem_from_list(states_it));
	  	states_it=get_next_from_list(states_it);

	  }

	  //attribution alphabet 
	  new_af->alphabet = alphab ;

	  return new_af;
}

/* affiche les etats et transis pour un AF DETERMINISTE comme suis : 
etat : 1 , successeurs :  (a, 2) (b, 3)
etat : 2 , succersseurs : (a, 4,) (b, 7)
etc.

*/

void print_composantes_accessibles(Automata* af){
	
	List_t* departure = (List_t*)get_elem_from_list(af->start) ;
	List_t* liste_marques = creer_liste_vide() ;
	List_t* debut_liste_marques = liste_marques;
	ajouter_element(liste_marques, get_elem_from_list(departure)); 

 	int nb_accessibles = 0;
	while(liste_marques != NULL){
		List_t* looked = liste_marques;
		List_t* transis_sortantes =( (State*)get_elem_from_list(looked))->transis;
		while(transis_sortantes != NULL){
			Transi* transi_looked = get_elem_from_list(transis_sortantes);
			State* state_arrivee = transi_looked->next;
			//regarder si l'état n'est pas déja marqué :
			List_t* it_checker = debut_liste_marques;
			bool state_marqued = false;
			while (it_checker != NULL){
				if (get_elem_from_list(it_checker) == state_arrivee){
					state_marqued = true;
				}
				it_checker=get_next_from_list(it_checker);
			}
			//fin du check
			if (state_marqued==false){
				ajouter_element(liste_marques, state_arrivee);
				nb_accessibles++;
			}
			transis_sortantes = get_next_from_list(transis_sortantes);
		}
		liste_marques=get_next_from_list(liste_marques);
	}
	printf("nombre d'accessibles depuis depart : %d \n", nb_accessibles );




}



 

void print_states_and_transis (Automata* af){
	List_t* states_it = af->states;
	State* state_looked = (State*)get_elem_from_list(states_it);
	int* state_number = (int*)get_elem_from_list(state_looked->first_nb);

	while (states_it != NULL ){
		printf("caca numero 1\n");
		state_looked = (State*)get_elem_from_list(states_it);
		state_number = (int*)get_elem_from_list(state_looked->first_nb);
		printf("cacaacaca\n");
		printf("%d st nber\n", *state_number );
		printf("caca numero 2\n");
  	printf("\netat : %d , successeurs :",*state_number);
  	printf("test apres \n");
  	//List_t* letters_it = (af->alphabet)->letters;
  	/*while(letters_it != NULL){
    		printf("(%c, %d) ",*(char*)get_elem_from_list(letters_it),*(int*)get_elem_from_list(delta(get_elem_from_list(states_it), letters_it) ));
    		letters_it = get_next_from_list(letters_it);
  	}*/
  	states_it = get_next_from_list(states_it);
	}
}

bool is_deterministe (Automata* a){
	if( get_next_from_list(a->start) != NULL ){
		printf("faux, trop d'etat initiaux\n") ; 
		return false ;
	}

	List_t* state_it = a->states ;


	while( state_it != NULL ){
		State* actual_state = (State*)get_elem_from_list(state_it);

		List_t* letter_it = (a->alphabet)->letters ;

		while( letter_it != NULL ){
			bool lab_exist = false ; 
			List_t* actual_state_transis_it = actual_state->transis ;
			while( actual_state_transis_it != NULL && lab_exist==false){
				if(((Transi*)get_elem_from_list(actual_state_transis_it))->label == letter_it)
					lab_exist = true ;
				actual_state_transis_it = get_next_from_list(actual_state_transis_it);
			}
			if( !lab_exist ){
				printf("la lettre n'a aucune transi \n");
				return false ;
			}
			letter_it = get_next_from_list(letter_it);
		}
		if( size_of_list(actual_state->transis) != size_of_list((a->alphabet)->letters) ){
			printf("trop de transi par rapport au nbre de lettre \n");
			return false ;
		}

		state_it = get_next_from_list(state_it) ;
	}
	return true ;
}