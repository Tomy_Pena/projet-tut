#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdbool.h>

typedef struct List_t List_t;


/* Crée une liste chainée vide (tosu les champs à NULL)
 * Note: appeler supprimer_liste() pour vider la mémoire allouée
 */
List_t* creer_liste_vide();


/**
 * Crée une liste avec un élément, le suivant étant positionné à NULL
 */
List_t* creer_liste(void* elem);

void supprimer_liste(List_t* l);

void supprimer_element(List_t* l, int n);

void ajouter_element(List_t* l, void *elem_to_add);

bool check_elem (List_t* l1, List_t* l2);

bool find_int_in_list(int k , List_t* l);

List_t* fusion_list(List_t* l, List_t* list_added);

void* get_elem_from_list(List_t* l);

List_t* get_next_from_list(List_t* l);

bool check_lists_equals (List_t* l1, List_t* l2);

int size_of_list(List_t* l);

#endif
